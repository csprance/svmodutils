local SvMu = require('GameSDK.Scripts.SvModUtils.SvModUtils')

local t = {
   {
      name = "Fred",
      address = "16 Long Street",
      phone = "123456"
   },

   {
      name = "Wilma",
      address = "16 Long Street",
      phone = "123456"
   },

   {
      name = "Barney",
      address = "17 Long Street",
      phone = "123457"
   }

}

SvMu.Log(SvMu.DebugTable(t))